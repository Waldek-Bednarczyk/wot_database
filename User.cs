﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOT_database
{
    class User
    {
        public int Privilege { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public User(int privilege, string login, string password)
        {
            Privilege = privilege;
            Login = login;
            Password = password;
        }
    }
}
