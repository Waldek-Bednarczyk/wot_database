﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using System.Drawing;

namespace WOT_database
{
    public partial class frmMainMenu : Form
    {
        BindingSource bindingSource = new BindingSource();
        String connectionString = "Data Source=DESKTOP-SF6PN64\\SQLEXPRESS;Initial Catalog=WOT_stats;Integrated Security=True";
        private int _formPrivilege;

        public frmMainMenu(int form)
        {
            InitializeComponent();
            _formPrivilege = form;
            if (_formPrivilege == 1)
            {
                buttonExecuteQuery.Visible = true;
                textBoxQuery.Visible = true;
                buttonAdd.Visible = true;
                buttonDelete.Visible = true;
                buttonEdit.Visible = true;
                label1.Visible = true;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle,
                                                                       Color.DeepSkyBlue,
                                                                       Color.YellowGreen,
                                                                       45F))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }

        private void DisplaySelected(int value)
        {
            //wybor tabeli do wyswietlenia
            switch (value)
            {
                case 0:
                    dataGridViewTable.DataSource = playersTableAdapter1.GetData();
                    break;
                case 1:
                    dataGridViewTable.DataSource = gamesTableAdapter1.GetData();
                    break;
                case 2:
                    dataGridViewTable.DataSource = match_ReportsTableAdapter1.GetData();
                    break;
                case 3:
                    dataGridViewTable.DataSource = mapsTableAdapter1.GetData();
                    break;
                case 4:
                    dataGridViewTable.DataSource = seasonsTableAdapter1.GetData();
                    break;
                case 5:
                    dataGridViewTable.DataSource = tanksTableAdapter1.GetData();
                    break;
                case 6:
                    dataGridViewTable.DataSource = tank_TypesTableAdapter1.GetData();
                    break;
                case 7:
                    dataGridViewTable.DataSource = nationsTableAdapter1.GetData();
                    break;
            }
        }

        private void buttonExecuteQuery_Click(object sender, EventArgs e)
        {
            String sqlCommand = textBoxQuery.Text;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand, connectionString);
            SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
            DataTable table = new DataTable();
            //obsluga blednego zapytania
            try
            {
                sqlDataAdapter.Fill(table);
                dataGridViewTable.DataSource = table;
                MessageBox.Show("Query executed correctly", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxQuery.Text = "";
            }
            catch { MessageBox.Show( "Query is not correct", "Incorrect query", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
        }

        private void buttonDisplaySelectedTable_Click(object sender, EventArgs e)
        {
            DisplaySelected(comboBoxTableSelect.SelectedIndex);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var row = dataGridViewTable.SelectedRows[0];
            int id = Convert.ToInt32(row.Cells[0].Value);

            String sqlCommand = "DELETE FROM " + comboBoxTableSelect.Text + " WHERE " + dataGridViewTable.Columns[0].Name + " = " + id;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand, connectionString);
            SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
            DataTable table = new DataTable();
            sqlDataAdapter.Fill(table);
            DisplaySelected(comboBoxTableSelect.SelectedIndex);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (comboBoxTableSelect.SelectedIndex == 0)
            {
                frmPlayer frmPlayer = new frmPlayer(0,-1);
                frmPlayer.ShowDialog();
            }
            else if (comboBoxTableSelect.SelectedIndex == 3)
            {
                frmMap frmMap = new frmMap(0,-1);
                frmMap.ShowDialog();
            }
            else if(comboBoxTableSelect.SelectedIndex == 5)
            {
                frmTank frmTank = new frmTank(0,-1);
                frmTank.ShowDialog();
            }
            else MessageBox.Show("Adding to this table is not permitted", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            DisplaySelected(comboBoxTableSelect.SelectedIndex);
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            var row = dataGridViewTable.SelectedRows[0];
            int rowNumber = Convert.ToInt32(row.Cells[0].Value);

            if (comboBoxTableSelect.SelectedIndex == 0)
            {
                frmPlayer frmPlayer = new frmPlayer(1,rowNumber);
                frmPlayer.ShowDialog();
            }
            else if (comboBoxTableSelect.SelectedIndex == 3)
            {
                frmMap frmMap = new frmMap(1,rowNumber);
                frmMap.ShowDialog();
            }
            else if (comboBoxTableSelect.SelectedIndex == 5)
            {
                frmTank frmTank = new frmTank(1,rowNumber);
                frmTank.Show();
            }
            else MessageBox.Show( "Editing in this table is not permitted", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            DisplaySelected(comboBoxTableSelect.SelectedIndex);
        }
    }
}
