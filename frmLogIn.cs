﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WOT_database
{
    public partial class frmLogIn : Form
    {
        private List<User> _users;
        private int _privilege = -1;
        public frmLogIn()
        {
            InitializeComponent();
            _users = new List<User>();
            _users.Add(new User(1, "admin", "admin"));
            _users.Add(new User(0, "user", "user"));
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle,
                                                                       Color.Blue,
                                                                       Color.Orange,
                                                                       135F))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            foreach (var user in _users)
            {
                if ((textBoxLogin.Text == user.Login) && (textBoxPassword.Text == user.Password))
                {
                    _privilege = user.Privilege;
                    break;
                }
            }
            textBoxLogin.Text = "";
            textBoxPassword.Text = "";
            if (_privilege == -1) MessageBox.Show("Incorrect Login or Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                frmMainMenu frmMain = new frmMainMenu(_privilege);
                frmMain.ShowDialog();
            }

        }
    }
}
