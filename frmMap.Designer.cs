﻿namespace WOT_database
{
    partial class frmMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label map_NameLabel;
            System.Windows.Forms.Label difficultyLabel;
            System.Windows.Forms.Label seasonIDLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMap));
            this.wOT_statsDataSet = new WOT_database.WOT_statsDataSet();
            this.mapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mapsTableAdapter = new WOT_database.WOT_statsDataSetTableAdapters.MapsTableAdapter();
            this.tableAdapterManager = new WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager();
            this.map_NameTextBox = new System.Windows.Forms.TextBox();
            this.difficultyTextBox = new System.Windows.Forms.TextBox();
            this.seasonIDTextBox = new System.Windows.Forms.TextBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            map_NameLabel = new System.Windows.Forms.Label();
            difficultyLabel = new System.Windows.Forms.Label();
            seasonIDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wOT_statsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // map_NameLabel
            // 
            map_NameLabel.AutoSize = true;
            map_NameLabel.BackColor = System.Drawing.Color.Transparent;
            map_NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            map_NameLabel.Location = new System.Drawing.Point(60, 113);
            map_NameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            map_NameLabel.Name = "map_NameLabel";
            map_NameLabel.Size = new System.Drawing.Size(114, 25);
            map_NameLabel.TabIndex = 3;
            map_NameLabel.Text = "Map Name:";
            // 
            // difficultyLabel
            // 
            difficultyLabel.AutoSize = true;
            difficultyLabel.BackColor = System.Drawing.Color.Transparent;
            difficultyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            difficultyLabel.Location = new System.Drawing.Point(60, 158);
            difficultyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            difficultyLabel.Name = "difficultyLabel";
            difficultyLabel.Size = new System.Drawing.Size(90, 25);
            difficultyLabel.TabIndex = 5;
            difficultyLabel.Text = "Difficulty:";
            // 
            // seasonIDLabel
            // 
            seasonIDLabel.AutoSize = true;
            seasonIDLabel.BackColor = System.Drawing.Color.Transparent;
            seasonIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            seasonIDLabel.Location = new System.Drawing.Point(60, 202);
            seasonIDLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            seasonIDLabel.Name = "seasonIDLabel";
            seasonIDLabel.Size = new System.Drawing.Size(110, 25);
            seasonIDLabel.TabIndex = 7;
            seasonIDLabel.Text = "Season ID:";
            // 
            // wOT_statsDataSet
            // 
            this.wOT_statsDataSet.DataSetName = "WOT_statsDataSet";
            this.wOT_statsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mapsBindingSource
            // 
            this.mapsBindingSource.DataMember = "Maps";
            this.mapsBindingSource.DataSource = this.wOT_statsDataSet;
            // 
            // mapsTableAdapter
            // 
            this.mapsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.GamesTableAdapter = null;
            this.tableAdapterManager.MapsTableAdapter = this.mapsTableAdapter;
            this.tableAdapterManager.Match_ReportsTableAdapter = null;
            this.tableAdapterManager.NationsTableAdapter = null;
            this.tableAdapterManager.PlayersTableAdapter = null;
            this.tableAdapterManager.SeasonsTableAdapter = null;
            this.tableAdapterManager.Tank_TypesTableAdapter = null;
            this.tableAdapterManager.TanksTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // map_NameTextBox
            // 
            this.map_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mapsBindingSource, "Map_Name", true));
            this.map_NameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.map_NameTextBox.Location = new System.Drawing.Point(201, 107);
            this.map_NameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.map_NameTextBox.Name = "map_NameTextBox";
            this.map_NameTextBox.Size = new System.Drawing.Size(171, 30);
            this.map_NameTextBox.TabIndex = 4;
            // 
            // difficultyTextBox
            // 
            this.difficultyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mapsBindingSource, "Difficulty", true));
            this.difficultyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.difficultyTextBox.Location = new System.Drawing.Point(201, 153);
            this.difficultyTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.difficultyTextBox.Name = "difficultyTextBox";
            this.difficultyTextBox.Size = new System.Drawing.Size(171, 30);
            this.difficultyTextBox.TabIndex = 6;
            // 
            // seasonIDTextBox
            // 
            this.seasonIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.mapsBindingSource, "SeasonID", true));
            this.seasonIDTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.seasonIDTextBox.Location = new System.Drawing.Point(201, 198);
            this.seasonIDTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.seasonIDTextBox.Name = "seasonIDTextBox";
            this.seasonIDTextBox.Size = new System.Drawing.Size(171, 30);
            this.seasonIDTextBox.TabIndex = 8;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonConfirm.Location = new System.Drawing.Point(140, 274);
            this.buttonConfirm.Margin = new System.Windows.Forms.Padding(4);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(167, 62);
            this.buttonConfirm.TabIndex = 9;
            this.buttonConfirm.Text = "Add";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // frmMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 403);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(map_NameLabel);
            this.Controls.Add(this.map_NameTextBox);
            this.Controls.Add(difficultyLabel);
            this.Controls.Add(this.difficultyTextBox);
            this.Controls.Add(seasonIDLabel);
            this.Controls.Add(this.seasonIDTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Map";
            ((System.ComponentModel.ISupportInitialize)(this.wOT_statsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WOT_statsDataSet wOT_statsDataSet;
        private System.Windows.Forms.BindingSource mapsBindingSource;
        private WOT_statsDataSetTableAdapters.MapsTableAdapter mapsTableAdapter;
        private WOT_statsDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox map_NameTextBox;
        private System.Windows.Forms.TextBox difficultyTextBox;
        private System.Windows.Forms.TextBox seasonIDTextBox;
        private System.Windows.Forms.Button buttonConfirm;
    }
}