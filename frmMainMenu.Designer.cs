﻿namespace WOT_database
{
    partial class frmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainMenu));
            this.buttonExecuteQuery = new System.Windows.Forms.Button();
            this.dataGridViewTable = new System.Windows.Forms.DataGridView();
            this.gamesTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.GamesTableAdapter();
            this.comboBoxTableSelect = new System.Windows.Forms.ComboBox();
            this.mapsTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.MapsTableAdapter();
            this.match_ReportsTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.Match_ReportsTableAdapter();
            this.nationsTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.NationsTableAdapter();
            this.playersTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.PlayersTableAdapter();
            this.seasonsTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.SeasonsTableAdapter();
            this.tableAdapterManager1 = new WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager();
            this.tank_TypesTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.Tank_TypesTableAdapter();
            this.tanksTableAdapter1 = new WOT_database.WOT_statsDataSetTableAdapters.TanksTableAdapter();
            this.buttonDisplaySelectedTable = new System.Windows.Forms.Button();
            this.textBoxQuery = new System.Windows.Forms.TextBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExecuteQuery
            // 
            this.buttonExecuteQuery.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonExecuteQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonExecuteQuery.Location = new System.Drawing.Point(1083, 186);
            this.buttonExecuteQuery.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonExecuteQuery.Name = "buttonExecuteQuery";
            this.buttonExecuteQuery.Size = new System.Drawing.Size(160, 54);
            this.buttonExecuteQuery.TabIndex = 1;
            this.buttonExecuteQuery.Text = "Execute Query";
            this.buttonExecuteQuery.UseVisualStyleBackColor = true;
            this.buttonExecuteQuery.Visible = false;
            this.buttonExecuteQuery.Click += new System.EventHandler(this.buttonExecuteQuery_Click);
            // 
            // dataGridViewTable
            // 
            this.dataGridViewTable.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTable.Location = new System.Drawing.Point(12, 11);
            this.dataGridViewTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewTable.Name = "dataGridViewTable";
            this.dataGridViewTable.ReadOnly = true;
            this.dataGridViewTable.RowTemplate.Height = 24;
            this.dataGridViewTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTable.Size = new System.Drawing.Size(948, 665);
            this.dataGridViewTable.TabIndex = 2;
            // 
            // gamesTableAdapter1
            // 
            this.gamesTableAdapter1.ClearBeforeFill = true;
            // 
            // comboBoxTableSelect
            // 
            this.comboBoxTableSelect.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBoxTableSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTableSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxTableSelect.FormattingEnabled = true;
            this.comboBoxTableSelect.Items.AddRange(new object[] {
            "Players",
            "Games",
            "Match_Reports",
            "Maps",
            "Seasons",
            "Tanks",
            "Tank_Types",
            "Nations"});
            this.comboBoxTableSelect.Location = new System.Drawing.Point(983, 386);
            this.comboBoxTableSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxTableSelect.Name = "comboBoxTableSelect";
            this.comboBoxTableSelect.Size = new System.Drawing.Size(165, 33);
            this.comboBoxTableSelect.TabIndex = 3;
            // 
            // mapsTableAdapter1
            // 
            this.mapsTableAdapter1.ClearBeforeFill = true;
            // 
            // match_ReportsTableAdapter1
            // 
            this.match_ReportsTableAdapter1.ClearBeforeFill = true;
            // 
            // nationsTableAdapter1
            // 
            this.nationsTableAdapter1.ClearBeforeFill = true;
            // 
            // playersTableAdapter1
            // 
            this.playersTableAdapter1.ClearBeforeFill = true;
            // 
            // seasonsTableAdapter1
            // 
            this.seasonsTableAdapter1.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.GamesTableAdapter = this.gamesTableAdapter1;
            this.tableAdapterManager1.MapsTableAdapter = this.mapsTableAdapter1;
            this.tableAdapterManager1.Match_ReportsTableAdapter = this.match_ReportsTableAdapter1;
            this.tableAdapterManager1.NationsTableAdapter = this.nationsTableAdapter1;
            this.tableAdapterManager1.PlayersTableAdapter = this.playersTableAdapter1;
            this.tableAdapterManager1.SeasonsTableAdapter = this.seasonsTableAdapter1;
            this.tableAdapterManager1.Tank_TypesTableAdapter = this.tank_TypesTableAdapter1;
            this.tableAdapterManager1.TanksTableAdapter = this.tanksTableAdapter1;
            this.tableAdapterManager1.UpdateOrder = WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tank_TypesTableAdapter1
            // 
            this.tank_TypesTableAdapter1.ClearBeforeFill = true;
            // 
            // tanksTableAdapter1
            // 
            this.tanksTableAdapter1.ClearBeforeFill = true;
            // 
            // buttonDisplaySelectedTable
            // 
            this.buttonDisplaySelectedTable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDisplaySelectedTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDisplaySelectedTable.Location = new System.Drawing.Point(1164, 374);
            this.buttonDisplaySelectedTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDisplaySelectedTable.Name = "buttonDisplaySelectedTable";
            this.buttonDisplaySelectedTable.Size = new System.Drawing.Size(160, 54);
            this.buttonDisplaySelectedTable.TabIndex = 4;
            this.buttonDisplaySelectedTable.Text = "Display";
            this.buttonDisplaySelectedTable.UseVisualStyleBackColor = true;
            this.buttonDisplaySelectedTable.Click += new System.EventHandler(this.buttonDisplaySelectedTable_Click);
            // 
            // textBoxQuery
            // 
            this.textBoxQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxQuery.Location = new System.Drawing.Point(1015, 47);
            this.textBoxQuery.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxQuery.Multiline = true;
            this.textBoxQuery.Name = "textBoxQuery";
            this.textBoxQuery.Size = new System.Drawing.Size(309, 122);
            this.textBoxQuery.TabIndex = 5;
            this.textBoxQuery.Visible = false;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAdd.Location = new System.Drawing.Point(1083, 471);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(160, 54);
            this.buttonAdd.TabIndex = 6;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Visible = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDelete.Location = new System.Drawing.Point(1083, 587);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(160, 54);
            this.buttonDelete.TabIndex = 7;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Visible = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEdit.Location = new System.Drawing.Point(1083, 529);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(160, 54);
            this.buttonEdit.TabIndex = 8;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Visible = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(1018, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Query Writer";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(978, 330);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "Choose a table to display";
            // 
            // frmMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1341, 689);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.textBoxQuery);
            this.Controls.Add(this.buttonDisplaySelectedTable);
            this.Controls.Add(this.comboBoxTableSelect);
            this.Controls.Add(this.dataGridViewTable);
            this.Controls.Add(this.buttonExecuteQuery);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmMainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonExecuteQuery;
        private WOT_statsDataSetTableAdapters.GamesTableAdapter gamesTableAdapter1;
        private System.Windows.Forms.DataGridView dataGridViewTable;
        private System.Windows.Forms.ComboBox comboBoxTableSelect;
        private WOT_statsDataSetTableAdapters.MapsTableAdapter mapsTableAdapter1;
        private WOT_statsDataSetTableAdapters.Match_ReportsTableAdapter match_ReportsTableAdapter1;
        private WOT_statsDataSetTableAdapters.NationsTableAdapter nationsTableAdapter1;
        private WOT_statsDataSetTableAdapters.PlayersTableAdapter playersTableAdapter1;
        private WOT_statsDataSetTableAdapters.SeasonsTableAdapter seasonsTableAdapter1;
        private WOT_statsDataSetTableAdapters.TableAdapterManager tableAdapterManager1;
        private WOT_statsDataSetTableAdapters.Tank_TypesTableAdapter tank_TypesTableAdapter1;
        private WOT_statsDataSetTableAdapters.TanksTableAdapter tanksTableAdapter1;
        private System.Windows.Forms.Button buttonDisplaySelectedTable;
        private System.Windows.Forms.TextBox textBoxQuery;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

