﻿namespace WOT_database
{
    partial class frmTank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label tank_NameLabel;
            System.Windows.Forms.Label tierLabel;
            System.Windows.Forms.Label nationIDLabel;
            System.Windows.Forms.Label tankTypeIDLabel;
            System.Windows.Forms.Label life_PointsLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTank));
            this.wOT_statsDataSet = new WOT_database.WOT_statsDataSet();
            this.tanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tanksTableAdapter = new WOT_database.WOT_statsDataSetTableAdapters.TanksTableAdapter();
            this.tableAdapterManager = new WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager();
            this.tank_NameTextBox = new System.Windows.Forms.TextBox();
            this.tierTextBox = new System.Windows.Forms.TextBox();
            this.nationIDTextBox = new System.Windows.Forms.TextBox();
            this.tankTypeIDTextBox = new System.Windows.Forms.TextBox();
            this.life_PointsTextBox = new System.Windows.Forms.TextBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            tank_NameLabel = new System.Windows.Forms.Label();
            tierLabel = new System.Windows.Forms.Label();
            nationIDLabel = new System.Windows.Forms.Label();
            tankTypeIDLabel = new System.Windows.Forms.Label();
            life_PointsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wOT_statsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tanksBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tank_NameLabel
            // 
            tank_NameLabel.AutoSize = true;
            tank_NameLabel.BackColor = System.Drawing.Color.Transparent;
            tank_NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            tank_NameLabel.Location = new System.Drawing.Point(53, 81);
            tank_NameLabel.Name = "tank_NameLabel";
            tank_NameLabel.Size = new System.Drawing.Size(120, 25);
            tank_NameLabel.TabIndex = 3;
            tank_NameLabel.Text = "Tank Name:";
            // 
            // tierLabel
            // 
            tierLabel.AutoSize = true;
            tierLabel.BackColor = System.Drawing.Color.Transparent;
            tierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            tierLabel.Location = new System.Drawing.Point(53, 122);
            tierLabel.Name = "tierLabel";
            tierLabel.Size = new System.Drawing.Size(52, 25);
            tierLabel.TabIndex = 5;
            tierLabel.Text = "Tier:";
            // 
            // nationIDLabel
            // 
            nationIDLabel.AutoSize = true;
            nationIDLabel.BackColor = System.Drawing.Color.Transparent;
            nationIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            nationIDLabel.Location = new System.Drawing.Point(53, 162);
            nationIDLabel.Name = "nationIDLabel";
            nationIDLabel.Size = new System.Drawing.Size(98, 25);
            nationIDLabel.TabIndex = 7;
            nationIDLabel.Text = "Nation ID:";
            // 
            // tankTypeIDLabel
            // 
            tankTypeIDLabel.AutoSize = true;
            tankTypeIDLabel.BackColor = System.Drawing.Color.Transparent;
            tankTypeIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            tankTypeIDLabel.Location = new System.Drawing.Point(53, 203);
            tankTypeIDLabel.Name = "tankTypeIDLabel";
            tankTypeIDLabel.Size = new System.Drawing.Size(137, 25);
            tankTypeIDLabel.TabIndex = 9;
            tankTypeIDLabel.Text = "Tank Type ID:";
            // 
            // life_PointsLabel
            // 
            life_PointsLabel.AutoSize = true;
            life_PointsLabel.BackColor = System.Drawing.Color.Transparent;
            life_PointsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            life_PointsLabel.Location = new System.Drawing.Point(53, 244);
            life_PointsLabel.Name = "life_PointsLabel";
            life_PointsLabel.Size = new System.Drawing.Size(108, 25);
            life_PointsLabel.TabIndex = 11;
            life_PointsLabel.Text = "Life Points:";
            // 
            // wOT_statsDataSet
            // 
            this.wOT_statsDataSet.DataSetName = "WOT_statsDataSet";
            this.wOT_statsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tanksBindingSource
            // 
            this.tanksBindingSource.DataMember = "Tanks";
            this.tanksBindingSource.DataSource = this.wOT_statsDataSet;
            // 
            // tanksTableAdapter
            // 
            this.tanksTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.GamesTableAdapter = null;
            this.tableAdapterManager.MapsTableAdapter = null;
            this.tableAdapterManager.Match_ReportsTableAdapter = null;
            this.tableAdapterManager.NationsTableAdapter = null;
            this.tableAdapterManager.PlayersTableAdapter = null;
            this.tableAdapterManager.SeasonsTableAdapter = null;
            this.tableAdapterManager.Tank_TypesTableAdapter = null;
            this.tableAdapterManager.TanksTableAdapter = this.tanksTableAdapter;
            this.tableAdapterManager.UpdateOrder = WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tank_NameTextBox
            // 
            this.tank_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tanksBindingSource, "Tank_Name", true));
            this.tank_NameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tank_NameTextBox.Location = new System.Drawing.Point(216, 78);
            this.tank_NameTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tank_NameTextBox.Name = "tank_NameTextBox";
            this.tank_NameTextBox.Size = new System.Drawing.Size(217, 30);
            this.tank_NameTextBox.TabIndex = 4;
            // 
            // tierTextBox
            // 
            this.tierTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tanksBindingSource, "Tier", true));
            this.tierTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tierTextBox.Location = new System.Drawing.Point(216, 118);
            this.tierTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tierTextBox.Name = "tierTextBox";
            this.tierTextBox.Size = new System.Drawing.Size(217, 30);
            this.tierTextBox.TabIndex = 6;
            // 
            // nationIDTextBox
            // 
            this.nationIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tanksBindingSource, "NationID", true));
            this.nationIDTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nationIDTextBox.Location = new System.Drawing.Point(216, 159);
            this.nationIDTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nationIDTextBox.Name = "nationIDTextBox";
            this.nationIDTextBox.Size = new System.Drawing.Size(217, 30);
            this.nationIDTextBox.TabIndex = 8;
            // 
            // tankTypeIDTextBox
            // 
            this.tankTypeIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tanksBindingSource, "TankTypeID", true));
            this.tankTypeIDTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tankTypeIDTextBox.Location = new System.Drawing.Point(216, 199);
            this.tankTypeIDTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tankTypeIDTextBox.Name = "tankTypeIDTextBox";
            this.tankTypeIDTextBox.Size = new System.Drawing.Size(217, 30);
            this.tankTypeIDTextBox.TabIndex = 10;
            // 
            // life_PointsTextBox
            // 
            this.life_PointsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tanksBindingSource, "Life_Points", true));
            this.life_PointsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.life_PointsTextBox.Location = new System.Drawing.Point(216, 240);
            this.life_PointsTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.life_PointsTextBox.Name = "life_PointsTextBox";
            this.life_PointsTextBox.Size = new System.Drawing.Size(217, 30);
            this.life_PointsTextBox.TabIndex = 12;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonConfirm.Location = new System.Drawing.Point(151, 298);
            this.buttonConfirm.Margin = new System.Windows.Forms.Padding(4);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(167, 62);
            this.buttonConfirm.TabIndex = 13;
            this.buttonConfirm.Text = "Add";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // frmTank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 450);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(tank_NameLabel);
            this.Controls.Add(this.tank_NameTextBox);
            this.Controls.Add(tierLabel);
            this.Controls.Add(this.tierTextBox);
            this.Controls.Add(nationIDLabel);
            this.Controls.Add(this.nationIDTextBox);
            this.Controls.Add(tankTypeIDLabel);
            this.Controls.Add(this.tankTypeIDTextBox);
            this.Controls.Add(life_PointsLabel);
            this.Controls.Add(this.life_PointsTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmTank";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tank";
            ((System.ComponentModel.ISupportInitialize)(this.wOT_statsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tanksBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WOT_statsDataSet wOT_statsDataSet;
        private System.Windows.Forms.BindingSource tanksBindingSource;
        private WOT_statsDataSetTableAdapters.TanksTableAdapter tanksTableAdapter;
        private WOT_statsDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox tank_NameTextBox;
        private System.Windows.Forms.TextBox tierTextBox;
        private System.Windows.Forms.TextBox nationIDTextBox;
        private System.Windows.Forms.TextBox tankTypeIDTextBox;
        private System.Windows.Forms.TextBox life_PointsTextBox;
        private System.Windows.Forms.Button buttonConfirm;
    }
}