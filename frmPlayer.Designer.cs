﻿namespace WOT_database
{
    partial class frmPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label last_NameLabel;
            System.Windows.Forms.Label first_NameLabel;
            System.Windows.Forms.Label email_addressLabel;
            System.Windows.Forms.Label statisticLabel;
            System.Windows.Forms.Label nickLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlayer));
            this.wOT_statsDataSet = new WOT_database.WOT_statsDataSet();
            this.playersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playersTableAdapter = new WOT_database.WOT_statsDataSetTableAdapters.PlayersTableAdapter();
            this.tableAdapterManager = new WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager();
            this.last_NameTextBox = new System.Windows.Forms.TextBox();
            this.first_NameTextBox = new System.Windows.Forms.TextBox();
            this.email_addressTextBox = new System.Windows.Forms.TextBox();
            this.statisticTextBox = new System.Windows.Forms.TextBox();
            this.nickTextBox = new System.Windows.Forms.TextBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            last_NameLabel = new System.Windows.Forms.Label();
            first_NameLabel = new System.Windows.Forms.Label();
            email_addressLabel = new System.Windows.Forms.Label();
            statisticLabel = new System.Windows.Forms.Label();
            nickLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wOT_statsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // last_NameLabel
            // 
            last_NameLabel.AutoSize = true;
            last_NameLabel.BackColor = System.Drawing.Color.Transparent;
            last_NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            last_NameLabel.Location = new System.Drawing.Point(53, 91);
            last_NameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            last_NameLabel.Name = "last_NameLabel";
            last_NameLabel.Size = new System.Drawing.Size(112, 25);
            last_NameLabel.TabIndex = 3;
            last_NameLabel.Text = "Last Name:";
            // 
            // first_NameLabel
            // 
            first_NameLabel.AutoSize = true;
            first_NameLabel.BackColor = System.Drawing.Color.Transparent;
            first_NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            first_NameLabel.Location = new System.Drawing.Point(53, 133);
            first_NameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            first_NameLabel.Name = "first_NameLabel";
            first_NameLabel.Size = new System.Drawing.Size(112, 25);
            first_NameLabel.TabIndex = 5;
            first_NameLabel.Text = "First Name:";
            // 
            // email_addressLabel
            // 
            email_addressLabel.AutoSize = true;
            email_addressLabel.BackColor = System.Drawing.Color.Transparent;
            email_addressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            email_addressLabel.Location = new System.Drawing.Point(53, 175);
            email_addressLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            email_addressLabel.Name = "email_addressLabel";
            email_addressLabel.Size = new System.Drawing.Size(141, 25);
            email_addressLabel.TabIndex = 7;
            email_addressLabel.Text = "Email address:";
            // 
            // statisticLabel
            // 
            statisticLabel.AutoSize = true;
            statisticLabel.BackColor = System.Drawing.Color.Transparent;
            statisticLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            statisticLabel.Location = new System.Drawing.Point(53, 217);
            statisticLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            statisticLabel.Name = "statisticLabel";
            statisticLabel.Size = new System.Drawing.Size(86, 25);
            statisticLabel.TabIndex = 9;
            statisticLabel.Text = "Statistic:";
            // 
            // nickLabel
            // 
            nickLabel.AutoSize = true;
            nickLabel.BackColor = System.Drawing.Color.Transparent;
            nickLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            nickLabel.Location = new System.Drawing.Point(53, 258);
            nickLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            nickLabel.Name = "nickLabel";
            nickLabel.Size = new System.Drawing.Size(56, 25);
            nickLabel.TabIndex = 11;
            nickLabel.Text = "Nick:";
            // 
            // wOT_statsDataSet
            // 
            this.wOT_statsDataSet.DataSetName = "WOT_statsDataSet";
            this.wOT_statsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // playersBindingSource
            // 
            this.playersBindingSource.DataMember = "Players";
            this.playersBindingSource.DataSource = this.wOT_statsDataSet;
            // 
            // playersTableAdapter
            // 
            this.playersTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.GamesTableAdapter = null;
            this.tableAdapterManager.MapsTableAdapter = null;
            this.tableAdapterManager.Match_ReportsTableAdapter = null;
            this.tableAdapterManager.NationsTableAdapter = null;
            this.tableAdapterManager.PlayersTableAdapter = this.playersTableAdapter;
            this.tableAdapterManager.SeasonsTableAdapter = null;
            this.tableAdapterManager.Tank_TypesTableAdapter = null;
            this.tableAdapterManager.TanksTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WOT_database.WOT_statsDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // last_NameTextBox
            // 
            this.last_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersBindingSource, "Last_Name", true));
            this.last_NameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.last_NameTextBox.Location = new System.Drawing.Point(216, 87);
            this.last_NameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.last_NameTextBox.Name = "last_NameTextBox";
            this.last_NameTextBox.Size = new System.Drawing.Size(209, 30);
            this.last_NameTextBox.TabIndex = 4;
            // 
            // first_NameTextBox
            // 
            this.first_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersBindingSource, "First_Name", true));
            this.first_NameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.first_NameTextBox.Location = new System.Drawing.Point(216, 129);
            this.first_NameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.first_NameTextBox.Name = "first_NameTextBox";
            this.first_NameTextBox.Size = new System.Drawing.Size(209, 30);
            this.first_NameTextBox.TabIndex = 6;
            // 
            // email_addressTextBox
            // 
            this.email_addressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersBindingSource, "Email_address", true));
            this.email_addressTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.email_addressTextBox.Location = new System.Drawing.Point(216, 171);
            this.email_addressTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.email_addressTextBox.Name = "email_addressTextBox";
            this.email_addressTextBox.Size = new System.Drawing.Size(209, 30);
            this.email_addressTextBox.TabIndex = 8;
            // 
            // statisticTextBox
            // 
            this.statisticTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersBindingSource, "Statistic", true));
            this.statisticTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.statisticTextBox.Location = new System.Drawing.Point(216, 213);
            this.statisticTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.statisticTextBox.Name = "statisticTextBox";
            this.statisticTextBox.Size = new System.Drawing.Size(209, 30);
            this.statisticTextBox.TabIndex = 10;
            // 
            // nickTextBox
            // 
            this.nickTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playersBindingSource, "Nick", true));
            this.nickTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nickTextBox.Location = new System.Drawing.Point(216, 255);
            this.nickTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nickTextBox.Name = "nickTextBox";
            this.nickTextBox.Size = new System.Drawing.Size(209, 30);
            this.nickTextBox.TabIndex = 12;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonConfirm.Location = new System.Drawing.Point(137, 327);
            this.buttonConfirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(167, 62);
            this.buttonConfirm.TabIndex = 13;
            this.buttonConfirm.Text = "Add";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // frmPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 477);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(last_NameLabel);
            this.Controls.Add(this.last_NameTextBox);
            this.Controls.Add(first_NameLabel);
            this.Controls.Add(this.first_NameTextBox);
            this.Controls.Add(email_addressLabel);
            this.Controls.Add(this.email_addressTextBox);
            this.Controls.Add(statisticLabel);
            this.Controls.Add(this.statisticTextBox);
            this.Controls.Add(nickLabel);
            this.Controls.Add(this.nickTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmPlayer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Player";
            ((System.ComponentModel.ISupportInitialize)(this.wOT_statsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playersBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WOT_statsDataSet wOT_statsDataSet;
        private System.Windows.Forms.BindingSource playersBindingSource;
        private WOT_statsDataSetTableAdapters.PlayersTableAdapter playersTableAdapter;
        private WOT_statsDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox last_NameTextBox;
        private System.Windows.Forms.TextBox first_NameTextBox;
        private System.Windows.Forms.TextBox email_addressTextBox;
        private System.Windows.Forms.TextBox statisticTextBox;
        private System.Windows.Forms.TextBox nickTextBox;
        private System.Windows.Forms.Button buttonConfirm;
    }
}