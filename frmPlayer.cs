﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WOT_database
{
    public partial class frmPlayer : Form
    {
        private int _editOrAdd;

        public frmPlayer (int editOrAdd, int rowNumber)
        {
            InitializeComponent();
            _editOrAdd = editOrAdd;
            if (editOrAdd == 1)
            {
                buttonConfirm.Text = "Edit";
                this.playersTableAdapter.FillByID(this.wOT_statsDataSet.Players, rowNumber);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle,
                                                                       Color.Green,
                                                                       Color.Orange,
                                                                       45F))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (_editOrAdd == 1)
            {
                this.Validate();
                this.playersBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.wOT_statsDataSet);
                this.Close();
            }
            else
            {
                this.Validate();
                this.playersTableAdapter.Insert(last_NameTextBox.Text,first_NameTextBox.Text,email_addressTextBox.Text,Convert.ToInt32(statisticTextBox.Text),nickTextBox.Text);
                this.Close();
            }
        }
    }
}
