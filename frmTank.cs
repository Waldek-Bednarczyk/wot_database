﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WOT_database
{
    public partial class frmTank : Form
    {
        private int _editOrAdd;

        public frmTank(int editOrAdd, int rowNumber)
        {
            InitializeComponent();
            _editOrAdd = editOrAdd;
            if(editOrAdd == 1)
            {
                buttonConfirm.Text = "Edit";
                this.tanksTableAdapter.FillByID(this.wOT_statsDataSet.Tanks,rowNumber);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle,
                                                                       Color.Green,
                                                                       Color.Orange,
                                                                       45F))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if(_editOrAdd == 1)
            {
                this.Validate();
                this.tanksBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.wOT_statsDataSet);
                this.Close();
            }
            else
            {
                this.Validate();
                tanksTableAdapter.Insert(tank_NameTextBox.Text, Convert.ToByte(tierTextBox.Text), Convert.ToByte(nationIDTextBox.Text), Convert.ToByte(tankTypeIDTextBox.Text), Convert.ToInt32(life_PointsTextBox.Text));
                this.Close();
            }
        }
    }
}
