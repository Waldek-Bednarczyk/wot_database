﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WOT_database
{
    public partial class frmMap : Form
    {
        private int _editOrAdd;


        public frmMap(int editOrAdd, int rowNumber)
        {
            InitializeComponent();
            _editOrAdd = editOrAdd;
            if (editOrAdd == 1)
            {
                buttonConfirm.Text = "Edit";
                this.mapsTableAdapter.FillByID(this.wOT_statsDataSet.Maps,rowNumber);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle,
                                                                       Color.Green,
                                                                       Color.Orange,
                                                                       45F))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (_editOrAdd == 1)
            {
                this.Validate();
                this.mapsBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.wOT_statsDataSet);
                this.Close();
            }
            else
            {
                this.Validate();
                this.mapsTableAdapter.Insert(map_NameTextBox.Text,Convert.ToByte(difficultyTextBox.Text),Convert.ToByte(seasonIDTextBox.Text));
                this.Close();
            }
        }
    }
}
